/*
  Admin routes - need to be authenticated as an admin to get this assets
*/

import config from '@/config/config'

// Should always be a bool - false, or string - 'admin'
let isAdminAuthRequired = config.ADMIN_AUTH === true ? 'admin' : false
if (process.env.NODE_ENV === 'production') isAdminAuthRequired = 'admin'

// Admin Dashboard
import AdminDashboard from '@/components/admin/dashboard/AdminDashboard'
/* Admin Dashboard Pages
====================================== */
import DashboardMyAccount from '@/views/admin/dashboard/my-account'
// Applicants
import DashboardApplicantsPage from '@/views/admin/dashboard/applicants'
import DashboardInviteUsers from '@/views/admin/dashboard/invite-users'
// Users
import DashboardUsersPage from '@/views/admin/dashboard/users'
import DashboardCreateUserPage from '@/views/admin/dashboard/create-user'
// Events
import DashboardScheduledEvents from '@/views/admin/dashboard/scheduled-events'
import DashboardCreateEventPage from '@/views/admin/dashboard/create-event'
import DashboardViewEventPage from '@/views/admin/dashboard/view-event'
// Travel
import DashboardTravelPage from '@/views/admin/dashboard/travel'
import DashboardEventTripsManage from '@/views/admin/dashboard/view-event-trips'
import DashboardViewTrip from '@/views/admin/dashboard/view-trip'

const routes = [
  {
		path: '/admin',
		// name: 'admin-home',
    redirect: '/admin/applicants',
    component: AdminDashboard,
		meta: { requiresAuth: isAdminAuthRequired },

		// Sub routes for Admin Dashboard
		children: [
			// Admin account page
			{
				name: 'admin-account',
        path: 'my-account',
        component: DashboardMyAccount,
				meta: {
					title: 'My account',
					requiresAuth: isAdminAuthRequired
				}
			},

			/* Applicants */
      {
				// Home/index route for admin dashboard
				name: 'admin-applicants-page',
        path: 'applicants',
				component: DashboardApplicantsPage,
				meta: {
					title: 'Applicants',
					requiresAuth: isAdminAuthRequired
				}
			},
			// Invite users page
			{
				name: 'admin-invite-users',
        path: 'invite-users',
        component: DashboardInviteUsers,
				meta: {
					title: 'Invite users',
					requiresAuth: isAdminAuthRequired
				}
			},
			
			// {
			// 	name: 'admin-applicants',
      //   path: 'applicants',
      //   component: DashboardApplicantsPage,
			// 	meta: {
			// 		title: 'Applicants',
			// 		requiresAuth: isAdminAuthRequired
			// 	}
			// },

			/* Users */
			{
				name: 'user-list-page',
        path: 'users',
        component: DashboardUsersPage,
				meta: {
					title: 'Users',
					requiresAuth: isAdminAuthRequired
				}
			},
			{
				name: 'create-user-page',
        path: 'create-user',
        component: DashboardCreateUserPage,
				meta: {
					title: 'Create User',
					requiresAuth: isAdminAuthRequired
				}
			},

			/* Events */
			{
				name: 'admin-scheduled-events-page',
        path: 'scheduled-events',
        component: DashboardScheduledEvents,
				meta: {
					title: 'Scheduled Events',
					requiresAuth: isAdminAuthRequired
				}
			},
			{
				name: 'admin-create-event-page',
        path: 'create-event',
        component: DashboardCreateEventPage,
				meta: {
					title: 'Create Event',
					requiresAuth: isAdminAuthRequired
				}
			},
			{
				name: 'admin-view-event-page',
        path: 'view-event',
        component: DashboardViewEventPage,
				meta: {
					title: 'View Event',
					requiresAuth: isAdminAuthRequired
				}
			},

			/* Travel */
			{
				name: 'admin-travel-page',
        path: 'travel',
        component: DashboardTravelPage,
				meta: {
					title: 'Travel',
					requiresAuth: isAdminAuthRequired
				}
			},
			{
				name: 'admin-event-trips-manage',
        path: 'event-trips-manage',
        component: DashboardEventTripsManage,
				meta: {
					title: 'Manage Trips',
					requiresAuth: isAdminAuthRequired
				}
			},
			{
				name: 'admin-view-trip',
        path: 'view-trip',
        component: DashboardViewTrip,
				meta: {
					title: 'Manage Trip',
					requiresAuth: isAdminAuthRequired
				}
			}
		]
  }
]


export default routes