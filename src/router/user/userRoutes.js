/*
  User routes - need to be authenticated as a user to get this assets
*/
// Config
import config from '@/config/config'

// Should be always be bool - false - or string 'user'
let isUserAuthRequired = config.USER_AUTH === true ? 'user' : false
if (process.env.NODE_ENV === 'production') isUserAuthRequired = 'user'

// User Dashboard
import UserDashboard from '@/components/user/dashboard/UserDashboard'
/* User Dashboard Pages
====================================== */
// User Dashboard pages
import UserDashboardAccount from '@/views/user/dashboard/account'
import UserDashboardEventInvitations from '@/views/user/dashboard/event-invitations'
import UserDashboardAttendAnEvent from '@/views/user/dashboard/apply-for-event'
import UserDashboardTravel from '@/views/user/dashboard/travel'
import UserDashboardAddTrip from '@/views/user/dashboard/add-trip-data'

const routes = [
  {
		// *import* generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited
		// for improving performance
		path: '/user',
		redirect: '/user/account',
		component: UserDashboard,
		meta: { requiresAuth: isUserAuthRequired },
		
		// Sub routes for User Dashboard
		children: [
      {
				// Home/index route for user dashboard
				name: 'user-account-page',
        path: 'account',
				component: UserDashboardAccount,
				meta: {
					title: 'Account',
					requiresAuth: isUserAuthRequired,
					accountPage: true
				}
			},

			/* Events */
			{
				name: 'user-event-invitations-page',
        path: 'event-invitations',
				component: UserDashboardEventInvitations,
				meta: {
					title: 'Event Invitations',
					requiresAuth: isUserAuthRequired
				}
			},
			{
				name: 'user-apply-for-event-page',
        path: 'apply-for-event',
				component: UserDashboardAttendAnEvent,
				meta: {
					title: 'Apply for Event',
					requiresAuth: isUserAuthRequired
				}
			},

			/* Travel */
			{
				name: 'user-travel-page',
        path: 'travel',
				component: UserDashboardTravel,
				meta: {
					title: 'Travel',
					requiresAuth: isUserAuthRequired
				}
			},
			{
				name: 'user-trips-form',
        path: 'add-trip-data',
				component: UserDashboardAddTrip,
				meta: {
					title: 'Add trip',
					requiresAuth: isUserAuthRequired
				}
			}
		]
	},
  { 
    path: '*',
    redirect: '/404'
  }
]

export default routes