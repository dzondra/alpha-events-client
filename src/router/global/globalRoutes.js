/*
  Global routes - don't need to be authenticated/logged in to get this assets
*/

const routes = [
	{
		path: '/',
		name: 'index',
		meta: {
			title: 'Home',
			requiresAuth: false
		},
		// route level code-splitting
		// this generates a separate chunk (home.[hash].js) for this route
		// which is lazy-loaded when the route is visited
		component: () => import(/* webpackChunkName: "index" */ '@/views/global/index.vue')
	},
	{
		path: '/apply',
		name: 'user-apply',
		meta: {
			title: 'Application',
			requiresAuth: false
		},
		component: () => import(/* webpackChunkName: "user-apply" */ '@/views/global/user-apply.vue')
	},
	{
		path: '/application-success',
		name: 'application-success',
		meta: {
			title: 'Thank you for applying',
			requiresAuth: false
		},
		component: () => import(/* webpackChunkName: "application-success" */ '@/views/global/application-success.vue')
	},
	{
		path: '/access',
		name: 'user-login',
		meta: {
			title: 'Login',
			requiresAuth: false,
			loginPage: true
		},
		component: () => import(/* webpackChunkName: "user-login" */ '@/views/global/user-login.vue')
	},
	{
		path: '/manage',
		name: 'admin-login',
		meta: {
			title: 'Admin Login',
			requiresAuth: false,
			loginPage: true
		},
		component: () => import(/* webpackChunkName: "admin-login" */ '@/views/global/admin-login.vue')
	},
	// {
	// 	path: '/logout',
	// 	name: 'logout',
	// 	meta: {
	// 		title: 'Logged out',
	// 		requiresAuth: false
	// 	},
	// 	component: () => import(/* webpackChunkName: "logout" */ '@/views/global/logout.vue')
	// },
	// 404 route (not found)
	{
		path: '/404',
		name: 'notFound',
		meta: {
			title: 'Not found',
			requiresAuth: false
		},
		component: () => import(/* webpackChunkName: "404" */ '@/views/global/404.vue')
	},
	
	// "Catch all route" - Needs to be the last route (redirects on 404 page)
	{
		path: '*',
		redirect: '/404',
		meta: {
			requiresAuth: false
		}
	}
]

export default routes