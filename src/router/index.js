import Vue from 'vue'
import Router from 'vue-router'
import globalRoutes from '@/router/global/globalRoutes'
import adminRoutes from '@/router/admin/adminRoutes'
import userRoutes from '@/router/user/userRoutes'

Vue.use(Router)

var allRoutes = []
allRoutes = allRoutes.concat(globalRoutes, adminRoutes, userRoutes)

const routes = allRoutes

export default new Router({
	mode: 'history',
	// TODO: Check if web server is configured to handle accessing routes
	// other than / (index) route
  base: process.env.BASE_URL || '/',
  routes: routes
})