import AuthenticationService from '@/services/AuthenticationService'

const state = {
	user: JSON.parse(localStorage.getItem('user')) || null,
	isUserProfileFinished: JSON.parse(localStorage.getItem('profile_finished')) || false
}

const actions = {
  setUser(context, user) {
		return new Promise((resolve, reject) => {
			localStorage.setItem('user', JSON.stringify(user))
			context.commit('setUser', user)
			resolve(true)
		})
	},
	// Remove all user related data
	logoutUser(context) {
		return new Promise((resolve, reject) => {
			// Remove data from localStorage
			localStorage.removeItem('user')
			localStorage.removeItem('profile_finished')
			localStorage.removeItem('viewing_event')
			localStorage.removeItem('viewing_attendance')
			localStorage.removeItem('viewing_trip')
			// Remove data from store
			context.commit('logoutUser')
			resolve(true)
		})
	},
	setIsUserProfileFinished(context, isUserProfileFinished) {
		return new Promise((resolve, reject) => {
			localStorage.setItem('profile_finished', isUserProfileFinished)
			context.commit('setIsUserProfileFinished', isUserProfileFinished)
			resolve(true)
		})
	},
}

const mutations = {
  setUser(state, user) {
		state.user = user
	},
	logoutUser(state) {
		state.user = null
		state.isUserProfileFinished = false
	},
	setIsUserProfileFinished(state, isUserProfileFinished) {
		state.isUserProfileFinished = isUserProfileFinished
	}
}

const getters = {
	// Returns currently logged in user object from the store
	getLoggedInUser(state) {
		return state.user
	},
	getIsFirstLogin(state) {
		return state.isFirstLogin
	},
	getIsUserProfileFinished(state) {
		return state.isUserProfileFinished
	}
}

export default {
  state,
  mutations,
	actions,
	getters
}