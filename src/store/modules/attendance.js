
const state = {
  currentlyViewingAttendance: localStorage.getItem('viewing_attendance') || null
}

const actions = {
	setCurrentlyViewingAttendance(context, eventId) {
		return new Promise((resolve, reject) => {
			localStorage.setItem('viewing_attendance', eventId)
			context.commit('setCurrentlyViewingAttendance', eventId)
			resolve(true)
		})
	},

	removeCurrentlyViewingAttendance(context) {
		localStorage.removeItem('viewing_attendance')
		context.commit('removeCurrentlyViewingAttendance')
	}
}

const mutations = {
	setCurrentlyViewingAttendance(state, eventId) {
		state.currentlyViewingAttendance = eventId
	},

	removeCurrentlyViewingAttendance(state) {
		state.currentlyViewingAttendance = null
	}
}

const getters = {
	getCurrentlyViewingAttendance(state) {
		return state.currentlyViewingAttendance
	}
}

export default {
  state,
  mutations,
	actions,
	getters
}