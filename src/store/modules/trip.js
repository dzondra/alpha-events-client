
const state = {
  currentlyViewingTrip: localStorage.getItem('viewing_trip') || null
}

const actions = {
	setCurrentlyViewingTrip(context, tripId) {
		return new Promise((resolve, reject) => {
			localStorage.setItem('viewing_trip', tripId)
			context.commit('setCurrentlyViewingTrip', tripId)
			resolve(true)
		})
	},

	removeCurrentlyViewingTrip(context) {
		localStorage.removeItem('viewing_trip')
		context.commit('removeCurrentlyViewingTrip')
	}
}

const mutations = {
	setCurrentlyViewingTrip(state, tripId) {
		state.currentlyViewingTrip = tripId
	},

	removeCurrentlyViewingTrip(state) {
		state.currentlyViewingTrip = null
	}
}

const getters = {
	getCurrentlyViewingTrip(state) {
		return state.currentlyViewingTrip
	}
}

export default {
  state,
  mutations,
	actions,
	getters
}