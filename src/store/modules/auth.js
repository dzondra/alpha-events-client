import AuthenticationService from '@/services/AuthenticationService'

const state = {
  token: localStorage.getItem('token') || null
}

const actions = {
  setToken(context, token) {
		return new Promise((resolve, reject) => {
			localStorage.setItem('token', token)
			context.commit('setToken', token)
			resolve(true)
			// console.log('Token set: ', token)
		})
	},
	destroyToken(context) {
		return new Promise((resolve, reject) => {
			localStorage.removeItem('token')
			context.commit('destroyToken')
			resolve(true)
			// console.log('Token destroyed.')
		})
	}
}

const mutations = {
  setToken(state, token) {
		state.token = token
	},
	destroyToken(state) {
		state.token = null
	}
}

const getters = {
	// Check if token is set (if user is logged in)
	isLoggedIn(state) {
		// TODO: Also check if token is real and valid token
		return state.token !== null
	},

	getUserToken(state) {
		return state.token
	}
}

export default {
  state,
  mutations,
	actions,
	getters
}