
const state = {
  currentlyViewingEvent: localStorage.getItem('viewing_event') || null
}

const actions = {
	setCurrentlyViewingEvent(context, eventId) {
		return new Promise((resolve, reject) => {
			localStorage.setItem('viewing_event', eventId)
			context.commit('setCurrentlyViewingEvent', eventId)
			resolve(true)
		})
	},

	removeCurrentlyViewingEvent(context) {
		localStorage.removeItem('viewing_event')
		context.commit('removeCurrentlyViewingEvent')
	}
}

const mutations = {
	setCurrentlyViewingEvent(state, eventId) {
		state.currentlyViewingEvent = eventId
	},

	removeCurrentlyViewingEvent(state) {
		state.currentlyViewingEvent = null
	}
}

const getters = {
	getCurrentlyViewingEvent(state) {
		return state.currentlyViewingEvent
	}
}

export default {
  state,
  mutations,
	actions,
	getters
}