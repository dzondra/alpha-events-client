import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
// import { sync } from 'vuex-router-sync'
import store from './store'
import './plugins'
// import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

/* Before each route
============================= */
router.beforeEach((to, from, next) => {
	// Handle page title
	let title = to.meta.title ? 'Alpha Events | ' + to.meta.title : 'Alpha Events'
	document.title = title
	
	// Redirect back from login page if user is logged in
	if (to.matched.some(record => record.meta.loginPage === true)) {
		const isLoggedIn = store.getters.isLoggedIn

		if (isLoggedIn) {
			router.go(-1)
			next(false)
		}
		else next()
	}

  else next()
})

/* Route guards
============================= */
// TODO: Check if a /forbidden route and view are needed
// or it should redirect to a 404

// Admin Route Guard
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth !== 'admin')) {
		// Routes that dont require 'admin' authentication
		next()

  } else {
		/* Admin routes that require authentication
		======================================== */
		const isLoggedIn = store.getters.isLoggedIn

		if (isLoggedIn) {
			const loggedInUser = store.getters.getLoggedInUser
			const isAdmin = loggedInUser.roles.includes('admin')

			if (isAdmin) next()
			else next('/forbidden')
		}

		else next('/forbidden')
  }
})

// User Route Guard
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth !== 'user')) {
		// Routes that dont require 'user' authentication
		next()
  } else {

		/* User routes that require authentication
		======================================== */
		const isLoggedIn = store.getters.isLoggedIn
		if (isLoggedIn) {

			const loggedInUser = store.getters.getLoggedInUser
			const isUser = loggedInUser.roles.includes('user')

			if (isUser) {
				// TODO: Remove if not in use
				// Redirect on finishAccountPage if user didnt finished account
				if (to.matched.some(record => record.meta.accountPage)) {
					return next()

				} else {
					const isUserAccountFinished  = store.getters.getIsUserProfileFinished

					if (isUserAccountFinished) {
						return next()
					} else {
						return next('/user/account')
					}
				}
			}

			// If not user
			else next('/forbidden')
		}
		
		// If not logged in
		else next('/forbidden')
  }
})

new Vue({
	router,
	store,
  render: h => h(App)
}).$mount('#app')
