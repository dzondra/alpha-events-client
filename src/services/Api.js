import axios from 'axios'
import config from '@/config/config'
import store from '@/store'
import router from '@/router'

const BASE_API_URL = config.BASE_API_URL

// TODO: Create /api directory, move to ResponseInterceptors file
const responseInterceptor = (err) =>  {
	return new Promise(function (resolve, reject) {
		if (err.response.status === 403 &&
			err.response.data.message === 'Access Forbidden' &&
			err.config &&
			!err.config.__isRetryRequest)
		{
			// Destroy token
			console.log('403, logging out...')
			store.dispatch('destroyToken')
			// Remove user and ALL user related data (from Store and localStorage)
			store.dispatch('logoutUser')
			// Redirect to login page
			router.push({ name: 'user-login' })
		}

		return reject(err)
	})
}

/*
	Instance used for sending all requests

	@param {Object} token - Holds token which is
	used to populate the Authorization header
*/

export default (token) => {
	const options = {
		baseURL: BASE_API_URL,
		timeout: 0, // 0 is unlimited timeout
		crossDomain: true
	}

	// If token is passed, add Authorization header
	if (token) {
		options.headers = {
			'Authorization': `Bearer ${token}`
		}
	}

	const instance = axios.create(options)

	// Force logout on user when api sends a response status 403
	// instance.interceptors.response.use(null, responseInterceptor)

	return instance
}
