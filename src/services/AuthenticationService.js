import Api from '@/services/Api'

export default {

	// Get Login form schema
	getLoginForm() {
		// Can add optional headers for each request
		// let config = {
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }
		return Api().get('/schema/login')
	},

  // Post Login
  postLogin(data) {
		let config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
		return Api().post('/login?include=user', data, config)
	},
	
	// Post Logout
  postLogout(data) {
		let config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
		return Api().post('/login', data, config)
  }
}