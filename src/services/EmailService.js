import Api from '@/services/Api'

export default {

	inviteUser(data, token) {
		return Api(token).post('/invite', data)
	}

} /* export default */
