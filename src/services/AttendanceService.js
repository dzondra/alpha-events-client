import Api from '@/services/Api'

export default {
	getUserAttendances(token) {
		return Api(token).get(`/attendance?match.status=pending`)
	},

	getAttendanceById(id, token) {
		return Api(token).get(`/attendance/${id}`)
	},

	createUserAttendance(data, token) {
		return Api(token).post(`/attendance`, data)
	},

	updateAttendance(data, token) {
		return Api(token).patch('/attendance', data)
	},

	addGuest(data, token) {
		return Api(token).post(`/attendance/${data.attendanceId}/guests/attachment?name=${data.guestName}`, data.file)
	},

	declineEventInvite(data, token) {
		return Api(token).patch(`/attendance`, data)
	},

	getAcceptedAttendancesAndEvents(token) {
		return Api(token).get('/attendance?match.status=approved&match.includesTrip=true&match.tripSubmitted=false&include=event')
	},

	getAttendancesAndTripsByEventId(eventId, token) {
		// TODO: Remove attendance data if not needed
		return Api(token).get(`/attendance?include=__trips_attendance_inverse&match.event=${eventId}`)
	},

	updateAttendance(data, token) {
		return Api(token).patch('/attendance', data)
	}

} /* export default */
