import Api from '@/services/Api'

export default {
  getModelFormSchema() {
		// Can add optional headers for each request
		// let config = {
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }
		return Api().get('/schema/models')
	},
	
	// Get model by USER id
	getModelAndFilesByUserId(userId, token) {
		return Api(token).get(`/models/?match.user=${userId}&include=photos`)
	},

	createModel(data, token) {
		return Api(token).post('/models', data)
	},

	async uploadModelFiles(previousFileId, file, fileMeta, modelId, token) {
		let config = {
			headers: {
				'Content-Type': 'binary/octet-stream',
			}
		}

		try {
			// If there was previous file, deleted it and then upload new
			if (previousFileId) {
				// Delete previous file
				const deleted = await this.deleteModelFileById(modelId, previousFileId, token)
				console.log('DELETED: ', deleted.status)

				if (deleted.status === 200) {
					console.log('UPLOADED')
					return Api(token).post(`/models/${modelId}/photos/attachment?name=${fileMeta}`, file, config)
				}

			// If there was not previous file, just upload the file
			} else {
				console.log('JUST UPLOADED...')
				return Api(token).post(`/models/${modelId}/photos/attachment?name=${fileMeta}`, file, config)
			}
	
		} catch (err) {
			return console.error(err)
		}
	},

	updateModel(data, token) {
		return Api(token).patch('/models', data)
	},

	deleteModelFileById(modelId, fileId, token) {
		return Api(token).delete(`/models/${modelId}/photos/attachment/${fileId}`)
	}


} /* export default */
