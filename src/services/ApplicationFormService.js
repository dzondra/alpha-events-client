import Api from '@/services/Api'
import axios from 'axios'

export default {
  getFormSchema() {
		// Can add optional headers for each request
		// let config = {
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }
		return Api().get('/schema/user')
  },

  // Submit Form
  submitForm(data) {
		let config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
    return Api().post('/user', data, config)
	},
	
	addMailwizzSubscriber(data) {
		let config = {
			headers: {
				'Content-Type': 'application/json'
			},
			crossDomain: true
		}
    return axios.post('https://store2.bestcashgigs.com/AlphaDelivered/operations/newSubscriber.php', data, config)
	}
}