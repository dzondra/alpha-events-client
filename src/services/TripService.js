import Api from '@/services/Api'

export default {
  getTripFormSchema() {
		// Can add optional headers for each request
		// let config = {
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }
		return Api().get('/schema/trips')
	},

	createTrip(data, token) {
		return Api(token).post('/trips', data)
	},

	// Returns all trips for logged in user,
	// but if admin sends the request it will return all Trip records
	getAllTrips(token) {
		return Api(token).get('/trips')
	},

	getTripById(id, token) {
		return Api(token).get(`/trips/${id}`)
	},

	getPendingTrips(token) {
		return Api(token).get('/trips?match.status=pending')
	},

	updateTripById(data, token) {
		return Api(token).patch(`/trips`, data)
	}

} /* export default */
