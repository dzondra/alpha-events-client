import Api from '@/services/Api'

export default {
  getEventFormSchema() {
		// Can add optional headers for each request
		// let config = {
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }
		return Api().get('/schema/events')
	},

	createEvent(data, token) {
		return Api(token).post('/events', data)
	},

	getAllEvents(token) {
		return Api(token).get(`/events`)
	},
	
	// Get Event by event id
	getEventById(eventId, token) {
		return Api(token).get(`/events/${eventId}`)
	},

	updateEvent(data, token) {
		return Api(token).patch(`/events`, data)
	},

	deleteEvent(eventId, token) {
		return Api(token).delete(`/events/${eventId}`)
	}

} /* export default */
