import Api from '@/services/Api'

export default {
  getUserFormSchema() {
		// Can add optional headers for each request
		// let config = {
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	}
		// }
		return Api().get('/schema/user')
	},

	getAllUsers(token) {
		return Api(token).get('/user')
	},

	getUserById(token, userId) {
		return Api(token).get(`/user/${userId}`)
	},

	getUninvitedUsers(eventId, token) {
		return Api(token).get(`/user/uninvited?match.event=${eventId}`)
	},

	// TODO: Not working (returns the whole user)
	// getUserFieldsById(userId, token) {
	// 	return Api(token).get(`/user/${userId},fields.login=true`)
	// },
	
  createUser(data, token) {
		let config = {
			headers: {
				'Content-Type': 'application/json',
			}
		}
		return Api(token).post('/user', data, config)
	},

	updateUser(data, token) {
		let config = {
			headers: {
				'Content-Type': 'application/json',
			}
		}
		return Api(token).patch(`/user`, data, config)
	},

	deleteUser(userId, token) {
		return Api(token).delete(`/user/${userId}`)
	},

	/* Files
	====================================================== */

	uploadUserFileById(userId, file, token) {
		let config = {
			headers: {
				'Content-Type': 'binary/octet-stream',
			}
		}
		return Api(token).post(`/user/attachment/${userId}`, file, config)
	},

	uploadFileForLoggedInUser(file, fileMeta, token) {
		let config = {
			headers: {
				'Content-Type': 'binary/octet-stream',
			}
		}
		return Api(token).post(`/user/attachment?name=${fileMeta}`, file, config)
	},

	getUserByIdAndFileMeta(userId, token) {
		return Api(token).get(`/user/${userId}?include=files,{"fields":{"name":true}}`)
	},

	deleteAttachmentById(id, token) {
		return Api(token).delete(`/user/attachment/${id}`)
	},
	
} /* export default */

// To create metadata for the file that is getting uploaded
// POST /user/attachment?name=foo

// To get attachment metadata for specific user
// GET /user/{id}?include=files
