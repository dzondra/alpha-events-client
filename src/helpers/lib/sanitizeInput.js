/*
	Remove empty strings, undefined and null values from input data and
	trim whitespaces.
	It accepts Object or an Array of Objects which will be sanitized.

	@param {(Object|Object[])} input - Usually input data from a form. Can be Object or Array.
*/
export default (input) => {
	// Check if input is array
	const isInputArray = Array.isArray(input)
	var inputData = input

	// If input is an Array
	if (isInputArray /*&& inputData.length > 0*/) {
		var output = []
		
		inputData.forEach(arrayElement => {
			var sanitizedObject = {}
			// const inputData = arrayElement

			for (let key in arrayElement) {
				if(arrayElement.hasOwnProperty(key) && arrayElement[key] !== undefined && arrayElement[key] !== null && arrayElement[key] !== '') {
					// console.log(`${key} : ${arrayElement[key]}`)
					let value
					typeof arrayElement[key] === 'string' ? value = arrayElement[key].trim() : value = arrayElement[key]
		
					// Append properties and values
					sanitizedObject[key] = value
				}
			}

			// Push sanitized object in the output array
			output.push(sanitizedObject)
		})

		return output

	// Input is an object
	} else {
		const inputData = input
		var outputData = {}
	
		for (let key in inputData) {
			if(inputData.hasOwnProperty(key) && inputData[key] !== undefined && inputData[key] !== null && inputData[key] !== '') {
				// console.log(`${key} : ${inputData[key]}`)
				let value
				typeof inputData[key] === 'string' ? value = inputData[key].trim() : value = inputData[key]
	
				// Append properties and values
				outputData[key] = value
			}
		}

		return outputData
	}
}