module.exports = {
  css: {
    loaderOptions: {
      scss: {
				prependData: `
          @import "~@/sass/_variables-components.scss";
          @import "~@/sass/_typo.scss";
        `
      }
    }
	},
	// transpileDependencies: [
  //   "vuetify"
  // ]
}